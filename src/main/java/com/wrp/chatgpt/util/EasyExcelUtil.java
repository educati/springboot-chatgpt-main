package com.wrp.chatgpt.util;


import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;


/**
 * @author WYP
 * @date 2022年08月15日 9:51
 */
public class EasyExcelUtil {
    private EasyExcelUtil(){
    }
    /**
     * 获取输出流
     *
     * @param response
     * @param fileName
     * @return
     * @throws IOException
     */
    public static ServletOutputStream getServletOutputStream(HttpServletResponse response, String fileName) throws IOException {
        fileName = URLEncoder.encode(fileName, "UTF-8");
        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("utf-8");
        // 设置浏览器不缓存
        response.setHeader("Pragma", "public");
        response.setHeader("Cache-Control", "no-store");
        response.addHeader("Cache-Control", "max-age=0");
        // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系

        response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".xlsx");
        return response.getOutputStream();
    }

}

