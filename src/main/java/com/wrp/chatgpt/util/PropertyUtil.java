package com.wrp.chatgpt.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class PropertyUtil {

    public static Properties getProperty(String proName) {
        Properties pro = new Properties();
        InputStream is = PropertyUtil.class.getClassLoader().getResourceAsStream(proName);
        try {
            pro.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return pro;
    }



}
