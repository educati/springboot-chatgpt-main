package com.wrp.chatgpt.config;

import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.config.annotation.*;
import org.springframework.web.socket.handler.WebSocketHandlerDecorator;
import org.springframework.web.socket.handler.WebSocketHandlerDecoratorFactory;
import org.springframework.web.socket.server.HandshakeInterceptor;

import java.util.Map;


/**
 * @author wrp
 */
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

    public static final Logger LOGGER = LoggerFactory.getLogger(WebSocketConfig.class);

    /**
     * 添加这个Endpoint，这样在网页中就可以通过websocket连接上服务,
     * 也就是我们配置websocket的服务地址,并且可以指定是否使用socketjs
     *
     * @param registry
     */
    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/ws")
                .addInterceptors(notifyHandshakeInterceptor())
                .withSockJS();

        registry.addEndpoint("/websocket")
                .setAllowedOrigins("http://localhost:8889")
                .addInterceptors()
                .withSockJS();
    }


    /**
     * 配置消息代理
     *
     * @param registry
     */
    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {

        /*
         *  "/app" 为配置应用服务器的地址前缀，表示所有以/app 开头的客户端消息或请求
         *  都会路由到带有@MessageMapping 注解的方法中
         */
        registry.setApplicationDestinationPrefixes("/app");


        // 自定义调度器，用于控制心跳线程
//        ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
//        // 线程池线程数，心跳连接开线程
//        taskScheduler.setPoolSize(1);
//        // 线程名前缀
//        taskScheduler.setThreadNamePrefix("websocket-heartbeat-thread-");
//        // 初始化
//        taskScheduler.initialize();


        /*
         * spring 内置broker对象
         * 1. 配置代理域，可以配置多个，此段代码配置代理目的地的前缀为 /topic
         *    我们就可以在配置的域上向客户端推送消息
         * 2，进行心跳设置，第一值表示server最小能保证发的心跳间隔毫秒数, 第二个值代码server希望client发的心跳间隔毫秒数
         * 3. 可以配置心跳线程调度器 setHeartbeatValue这个不能单独设置，不然不起作用，要配合setTaskScheduler才可以生效
         *    调度器我们可以自己写一个，也可以自己使用默认的调度器 new DefaultManagedTaskScheduler()
         */
        registry.enableSimpleBroker("/topic");
//                .setHeartbeatValue(new long[]{10000, 10000})
//                .setTaskScheduler(taskScheduler);
    }



    /**
     * 实施定制来断开会话
     * @param registration
     */
    @Override
    public void configureWebSocketTransport(final WebSocketTransportRegistration registration) {
        registration.addDecoratorFactory(new WebSocketHandlerDecoratorFactory() {
            @Override
            public WebSocketHandler decorate(final WebSocketHandler handler) {
                return new WebSocketHandlerDecorator(handler) {

                    @Override
                    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
                        LOGGER.warn("会话过程出现错误，关闭连接");
                        LOGGER.warn(exception.getMessage());
                        session.close();
                        super.handleTransportError(session, exception);
                    }
                };
            }
        });

    }




    /**
     * WebSocket 握手拦截器
     * 可做一些用户认证拦截处理
     */
    private HandshakeInterceptor notifyHandshakeInterceptor() {
        return new HandshakeInterceptor() {
            /**
             * websocket握手连接
             *
             * @return 返回是否同意握手
             */
            @Override
            public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse response,
                                           WebSocketHandler wsHandler, Map<String, Object> attributes) throws Exception {
                ServletServerHttpRequest req = (ServletServerHttpRequest) request;
                System.out.println(JSON.toJSONString(req.getURI()));
                System.out.println(req.getBody());
                System.out.println(JSON.toJSONString(attributes));
                System.out.println(JSON.toJSONString(req.getServletRequest().getParameterMap()));
//                //通过url的query参数获取认证参数
//                String token = req.getServletRequest().getParameter("token");
//                LOGGER.info("用户身份token:{}",token);
//                //根据token认证用户，不通过返回拒绝握手
//                Principal user = authenticate(token);
//                if(user == null){
//                    LOGGER.warn("用户身份token:{}，身份验证错误或者用户未登录！",token);
//                    return false;
//                }
//                //保存认证用户
//                attributes.put("user", user);
                return true;
            }

            @Override
            public void afterHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler, Exception exception) {
                //LOGGER.warn(exception.getMessage());
            }
        };

    }
}
