package com.wrp.chatgpt.config;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author wrp
 */
@Configuration
@ConfigurationProperties(prefix = "gpt")
public class GptConfig {

    private String token;


    private String apiKey;


    private String llModel;


    private String ip;


    private int port;


    private Boolean useProxy;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getLlModel() {
        return llModel;
    }

    public void setLlModel(String llModel) {
        this.llModel = llModel;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public Boolean getUseProxy() {
        return useProxy;
    }

    public void setUseProxy(Boolean useProxy) {
        this.useProxy = useProxy;
    }
}
