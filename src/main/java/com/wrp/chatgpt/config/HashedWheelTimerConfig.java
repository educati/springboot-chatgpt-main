package com.wrp.chatgpt.config;

import io.netty.util.HashedWheelTimer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

@Configuration
public class HashedWheelTimerConfig {

    @Bean("hashedWheelTimer")
    public HashedWheelTimer hashedWheelTimer(){
        return new HashedWheelTimer (100, TimeUnit.MILLISECONDS, 512);
    }
}
