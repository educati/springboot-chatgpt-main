package com.wrp.chatgpt;



import com.wrp.chatgpt.modules.gateway.SessionServer;
import io.netty.channel.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


/**
 *
 * @author wrp
 */
@SpringBootApplication
@EnableScheduling
@EnableAsync
public class App {



    public static void main(String[] args) throws Exception {

//        SessionServer server = new SessionServer();
//
//        Future<Channel> future = Executors.newFixedThreadPool(2).submit(server);
//        Channel channel = future.get();
//
//        if (null == channel) {
//            throw new RuntimeException("netty server start error channel is null");
//        }
//
//        while (!channel.isActive()) {
//            System.out.println("启动中");
//            Thread.sleep(500);
//        }
//        System.out.println("\"NettyServer启动服务完成");
//
//
//        Thread.sleep(Long.MAX_VALUE);
        SpringApplication.run(App.class, args);


    }





}
