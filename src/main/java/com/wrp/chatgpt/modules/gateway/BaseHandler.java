package com.wrp.chatgpt.modules.gateway;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author 小傅哥，微信：fustack
 * @description 数据处理器基累
 * @github github.com/fuzhengwei
 * @copyright 公众号：bugstack虫洞栈 | 博客：bugstack.cn - 沉淀、分享、成长，让自己和他人都能有所收获！
 */
public abstract class BaseHandler<T> extends SimpleChannelInboundHandler<T> {
    private final Logger logger = LoggerFactory.getLogger(BaseHandler.class);
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, T msg) throws Exception {
        logger.info("11111111111111111111");
        session(ctx, ctx.channel(), msg);
    }

    protected abstract void session(ChannelHandlerContext ctx, final Channel channel, T request);

}
