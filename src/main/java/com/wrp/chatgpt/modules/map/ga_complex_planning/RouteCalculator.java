package com.wrp.chatgpt.modules.map.ga_complex_planning;

public interface RouteCalculator {
    double routeLength(double fromX, double fromY, double destX, double destY);
}
