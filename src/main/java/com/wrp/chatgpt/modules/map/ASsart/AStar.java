package com.wrp.chatgpt.modules.map.ASsart;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.math.MathUtil;
import com.alibaba.fastjson.JSON;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.PriorityBlockingQueue;



/**
 * 利用A*算法计算路径
 * 参考博客：https://blog.csdn.net/weixin_51545953/article/details/131144000?utm_medium=distribute.pc_relevant.none-task-blog-2~default~baidujs_baidulandingword~default-0-131144000-blog-121815065.235^v38^pc_relevant_anti_vip_base&spm=1001.2101.3001.4242.1&utm_relevant_index=3
 *
 * @author wrp
 */
@Data
public class AStar {
    // 0是路 1是墙 2是起点 3是终点
    private int[][] map;
    // 起点坐标
    int startI;
    int startJ;
    // 终点坐标
    int endI;
    int endJ;

    public AStar(int[][] map) {
        this.map = map;
        // 获取起点和终点坐标
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                if(map[i][j]==2){
                    startI = i;
                    startJ = j;
                }
                if(map[i][j]==3){
                    endI = i;
                    endJ = j;
                }
            }
        }
    }

    public void solve(){
        boolean[][] active = new boolean[map.length][map[0].length];
        PriorityBlockingQueue<Node> priorityBlockingQueue = new PriorityBlockingQueue<>();
        // 从起点出发
        ArrayList<int[]> initPath = new ArrayList<>();
        initPath.add(new int[]{startI,startJ});
        priorityBlockingQueue.add(new Node(startI,startJ,initPath,calculateDistance(startI,startJ,initPath.size())));
        active[startI][startJ] = true;
        // 开始循环
        while (!priorityBlockingQueue.isEmpty()){
            Node node = priorityBlockingQueue.poll();
            if(node.getI()==endI && node.getJ()==endJ){
                for (int[] p : node.getPath()) {
                    System.out.println(Arrays.toString(p));
                }
                System.out.println("最短路长度为："+node.getPath().size());
                break;
            }
            // 向四周进行扩充
            // 上
            int i1 = node.getI()-1;
            int j1 = node.getJ();
            if(isAble(i1,j1,active)){
                ArrayList<int[]> path = new ArrayList<>(node.getPath());
                path.add(new int[]{i1,j1});
                priorityBlockingQueue.add(new Node(i1,j1,path,calculateDistance(i1,j1,path.size())));
                active[i1][j1] = true;
            }
            // 下
            int i2 = node.getI()+1;
            int j2 = node.getJ();
            if(isAble(i2,j2,active)){
                ArrayList<int[]> path = new ArrayList<>(node.getPath());
                path.add(new int[]{i2,j2});
                priorityBlockingQueue.add(new Node(i2,j2,path,calculateDistance(i2,j2,path.size())));
                active[i2][j2] = true;
            }
            // 左
            int i3 = node.getI();
            int j3 = node.getJ()-1;
            if(isAble(i3,j3,active)){
                ArrayList<int[]> path = new ArrayList<>(node.getPath());
                path.add(new int[]{i3,j3});
                priorityBlockingQueue.add(new Node(i3,j3,path,calculateDistance(i3,j3,path.size())));
                active[i3][j3] = true;
            }
            // 右
            int i4 = node.getI();
            int j4 = node.getJ()+1;
            if(isAble(i4,j4,active)){
                ArrayList<int[]> path = new ArrayList<>(node.getPath());
                path.add(new int[]{i4,j4});
                priorityBlockingQueue.add(new Node(i4,j4,path,calculateDistance(i4,j4,path.size())));
                active[i4][j4] = true;
            }
        }
    }
    // 判断坐标是否可行
    private boolean isAble(int i,int j,boolean[][] active){
        if(i<0 || i>=map.length){
            return false;
        }
        if(j<0 || j>= map[0].length){
            return false;
        }
        if(map[i][j]==1 || active[i][j]){
            return false;
        }
        return true;
    }
    // 计算距离终点的曼哈顿
    private int calculateDistance(int i,int j,int pathSize){
        return Math.abs(i-endI)+Math.abs(j-endJ) +pathSize;
    }
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    class Node implements Comparable<Node>{
        int i;
        int j;
        List<int[]> path;
        // 距离终点的曼哈顿距离
        int lenToEnd;
        @Override
        public int compareTo(Node o) {
            return Integer.compare(lenToEnd+path.size(),o.lenToEnd+o.path.size());
        }

        @Override
        public String toString() {
            return "Node{" +
                    "i=" + i +
                    ", j=" + j +
                    ", lenToEnd=" + lenToEnd +
                    '}';
        }
    }

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        int[][] map = new int[][]{
                {1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0},
                {0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0},
                {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0},
                {0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
                {0, 1, 1, 1, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0},
                {0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0},
                {0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0},
                {0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0},
                {0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0},
                {0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0},
                {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0},
                {0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0},
                {0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0},
                {0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0},
                {0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0},
                {0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 1},
                {0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1},
                {0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1},
                {0, 0, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 0, 0, 0, 0, 0, 0, 0},
        };
        new AStar(map).solve();
        System.out.println("用时："+(System.currentTimeMillis()-start)+"ms");

    }
}
