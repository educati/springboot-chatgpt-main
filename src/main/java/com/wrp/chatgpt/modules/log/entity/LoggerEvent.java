package com.wrp.chatgpt.modules.log.entity;


/**
 * 进程日志事件内容载体
 * @author  wrp
 */
public class LoggerEvent {

    private LoggerMessage log;

    public LoggerMessage getLog() {
        return log;
    }

    public void setLog(LoggerMessage log) {
        this.log = log;
    }
}
