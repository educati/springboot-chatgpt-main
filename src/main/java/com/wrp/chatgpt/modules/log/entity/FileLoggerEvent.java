package com.wrp.chatgpt.modules.log.entity;


/**
 * 文件日志事件内容载体
 *
 * @author wrp
 */
public class FileLoggerEvent {
    private String log;

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }
}
