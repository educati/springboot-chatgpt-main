package com.wrp.chatgpt.modules.log.factory;

import com.lmax.disruptor.EventFactory;
import com.wrp.chatgpt.modules.log.entity.LoggerEvent;


/**
 * 进程日志事件工厂类
 * @author wrp
 */
public class LoggerEventFactory implements EventFactory<LoggerEvent> {
    @Override
    public LoggerEvent newInstance() {
        return new LoggerEvent();
    }
}
