package com.wrp.chatgpt.modules.log.factory;

import com.lmax.disruptor.EventFactory;
import com.wrp.chatgpt.modules.log.entity.FileLoggerEvent;



/**
 * 文件日志事件工厂类
 * @author wrp
 */
public class FileLoggerEventFactory implements EventFactory<FileLoggerEvent> {
    @Override
    public FileLoggerEvent newInstance() {
        return new FileLoggerEvent();
    }
}
