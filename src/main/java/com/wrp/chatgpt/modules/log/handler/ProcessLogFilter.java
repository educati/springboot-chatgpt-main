package com.wrp.chatgpt.modules.log.handler;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.filter.Filter;
import ch.qos.logback.core.spi.FilterReply;
import com.wrp.chatgpt.modules.log.disruptor.LoggerDisruptorQueue;
import com.wrp.chatgpt.modules.log.entity.LoggerMessage;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.text.DateFormat;

/**
 *  <filter class="com.wrp.chatgpt.modules.log.handler.ProcessLogFilter"></filter>
 *  读取日志流的输出
 * @author wrp
 */
@Service
public class ProcessLogFilter extends Filter<ILoggingEvent> {

    @Override
    public FilterReply decide(ILoggingEvent event) {
        LoggerMessage loggerMessage = new LoggerMessage(
                event.getMessage()
                , DateFormat.getDateTimeInstance().format(new Date(event.getTimeStamp())),
                event.getThreadName(),
                event.getLoggerName(),
                event.getLevel().levelStr
        );
        LoggerDisruptorQueue.publishEvent(loggerMessage);
        return FilterReply.ACCEPT;
    }
}  