package com.wrp.chatgpt.modules.log.disruptor;


import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.dsl.Disruptor;
import com.wrp.chatgpt.modules.log.entity.FileLoggerEvent;
import com.wrp.chatgpt.modules.log.entity.LoggerEvent;
import com.wrp.chatgpt.modules.log.entity.LoggerMessage;
import com.wrp.chatgpt.modules.log.handler.FileLoggerEventHandler;
import com.wrp.chatgpt.modules.log.handler.LoggerEventHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.wrp.chatgpt.modules.log.factory.LoggerEventFactory;
import com.wrp.chatgpt.modules.log.factory.FileLoggerEventFactory;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;


/**
 * Disruptor 环形队列
 * @author wrp
 */
@Component
public class LoggerDisruptorQueue {

    private Executor executor = Executors.newCachedThreadPool();


    /**
     * 日志事件工厂
     */
    private LoggerEventFactory factory = new LoggerEventFactory();

    /**
     * 文件流事件工厂
     */
    private FileLoggerEventFactory fileLoggerEventFactory = new FileLoggerEventFactory();


    /**
     * 流大小
     */
    private int bufferSize = 2 * 1024;


    /**
     * 日志事件队列
     */
    private Disruptor<LoggerEvent> disruptor = new Disruptor<>(factory, bufferSize, executor);;

    /**
     * 工厂事件队列
     */
    private Disruptor<FileLoggerEvent> fileLoggerEventDisruptor = new Disruptor<>(fileLoggerEventFactory, bufferSize, executor);;

    private static  RingBuffer<LoggerEvent> ringBuffer;

    private static  RingBuffer<FileLoggerEvent> fileLoggerEventRingBuffer;

    @Autowired
    LoggerDisruptorQueue(LoggerEventHandler eventHandler, FileLoggerEventHandler fileLoggerEventHandler) {
        disruptor.handleEventsWith(eventHandler);
        fileLoggerEventDisruptor.handleEventsWith(fileLoggerEventHandler);
        ringBuffer = disruptor.getRingBuffer();
        fileLoggerEventRingBuffer = fileLoggerEventDisruptor.getRingBuffer();
        disruptor.start();
        fileLoggerEventDisruptor.start();
    }

    public static void publishEvent(LoggerMessage log) {
         //获取下一个sequence
        long sequence = ringBuffer.next();
        try {
            // Get the entry in the Disruptor
            LoggerEvent event = ringBuffer.get(sequence);
            // for the sequence  // Fill with data
            event.setLog(log);
        } finally {
            ringBuffer.publish(sequence);
        }
    }

    public static void publishEvent(String log) {
        if(fileLoggerEventRingBuffer == null) return;
        // Grab the next sequence
        long sequence = fileLoggerEventRingBuffer.next();
        try {
            // Get the entry in the Disruptor
            FileLoggerEvent event = fileLoggerEventRingBuffer.get(sequence);
            // for the sequence // Fill with data
            event.setLog(log);
        } finally {
            fileLoggerEventRingBuffer.publish(sequence);
        }
    }

}
