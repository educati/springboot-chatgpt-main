package com.wrp.chatgpt.modules.log.handler;

import com.lmax.disruptor.EventHandler;
import com.wrp.chatgpt.modules.log.entity.FileLoggerEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;


/**
 * 文件日志事件处理器
 * @author wrp
 */
@Component
public class FileLoggerEventHandler implements EventHandler<FileLoggerEvent> {

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @Override
    public void onEvent(FileLoggerEvent fileLoggerEvent, long l, boolean b) {
        messagingTemplate.convertAndSend("/topic/pullFileLogger",fileLoggerEvent.getLog());
    }
}
