package com.wrp.chatgpt.modules.gpt.entity;

public class ChatMessage {
	private MessageType type;
	private String content;
	private String sender;
    private String password;
    private String random;
	public ChatMessage() {
	}

	public String getRandom() {
		return random;
	}

	public void setRandom(String random) {
		this.random = random;
	}

	public ChatMessage(String content) {
		this.type = MessageType.CHAT;
		this.content =content;
		this.sender = "chatGPT";
	}

	public enum MessageType {
		CHAT, JOIN, LEAVE
	}

	public MessageType getType() {
		return type;
	}

	public void setType(MessageType type) {
		this.type = type;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getSender() {
		return sender;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	@Override
	public String toString() {
		return "ChatMessage{" + "type=" + type + ", content='" + content + '\'' + ", sender='" + sender + '\'' + '}';
	}
}
