package com.wrp.chatgpt.modules.gpt.service.impl;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.hutool.http.HttpRequest;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wrp.chatgpt.config.GptConfig;
import com.wrp.chatgpt.modules.gpt.service.ChatGptService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ChatGPTServiceImpl implements ChatGptService {
	 private static final Logger LOGGER = LoggerFactory.getLogger(ChatGPTServiceImpl.class);

	 @Autowired
	 private GptConfig gptConfig;


	/**
	 * 处理前端发送的消息，请求chatGpt 接口
	 * @param chatContent
	 * @return
	 */
	    @Override
		public String message(String chatContent) {
			System.setProperty("https.protocols", "TLSv1.2,TLSv1.1,SSLv3");

			Map<String, String> headers = new HashMap<>();

			headers.put("Content-Type","application/json");
			headers.put("Authorization","Bearer " + gptConfig.getApiKey());


			Proxy proxy = new Proxy(Proxy.Type.HTTP,new InetSocketAddress(gptConfig.getIp(), gptConfig.getPort()));

			HashMap<String, Object> params = new HashMap<>();
			params.put("model",gptConfig.getLlModel());

			List<HashMap<String, String>> maps = new ArrayList<>();
			HashMap<String, String> messages = new HashMap<>();
			messages.put("role","system");
			messages.put("content",chatContent);
			maps.add(messages);
			params.put("messages",maps);

			String paramJson = JSON.toJSONString(params);

			LOGGER.info("请求报文：{}",paramJson);
			String result = HttpRequest.post("https://api.openai.com/v1/chat/completions")
					.headerMap(headers, false)
					.body(paramJson)
					.setProxy(proxy)
					.execute()
					.body();
			JSONObject jsonObject = JSON.parseObject(result);
			LOGGER.info("返回报文：{}",jsonObject);
			JSONArray jsonArray = JSON.parseArray(jsonObject.getString("choices"));
			JSONObject jsonObjec = jsonArray.getJSONObject(0);
            JSONObject k = JSON.parseObject(jsonObjec.getString("message"));
	    	return k.getString("content");
	    }

}
