package com.wrp.chatgpt.modules.gpt.service;

import java.io.IOException;

/**
 * 处理前端
 * @author wrp
 */
public interface ChatGptService {

    /**
     * 处理前端发送的消息，请求chatGpt 接口
     * @param chatContent
     * @return
     */
    String message(String chatContent) throws IOException;
}
