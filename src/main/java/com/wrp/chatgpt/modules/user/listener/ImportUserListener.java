package com.wrp.chatgpt.modules.user.listener;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.fastjson.JSON;
import com.wrp.chatgpt.modules.user.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 导入监听
 *
 * @author wrp
 */
public class ImportUserListener extends AnalysisEventListener<User> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImportUserListener.class);


    private String cacheKey;

    List<User> users = new ArrayList<>();

    public static Map<String,List<User>> cache = new HashMap<>();


    public ImportUserListener(String cacheKey) {
        this.cacheKey = cacheKey;
    }

    /**
     * 每解析一条数据的监听
     * @param user
     * @param analysisContext
     */
    @Override
    public void invoke(User user, AnalysisContext analysisContext) {
        LOGGER.info("解析到一条数据:{}", JSON.toJSONString(user));
        users.add(user);
    }


    /**
     * 解析结束之后的监听
     * @param analysisContext
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        LOGGER.info("{}条数据，存储数据数据！", users.size());
        if (CollectionUtil.isEmpty(users)) {
            throw new RuntimeException("导入失败，Excel没有数据！");
        }
        cache.put(cacheKey, users);

        LOGGER.info("所有数据解析完成");
    }
}
