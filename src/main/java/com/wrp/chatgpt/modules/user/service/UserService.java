package com.wrp.chatgpt.modules.user.service;

import com.wrp.chatgpt.modules.gpt.entity.ChatMessage;
import com.wrp.chatgpt.modules.user.entity.User;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 用户服务
 *
 * @author wrp
 */
public interface UserService {

    /**
     * 判断用户是否具有使用行为
     *
     * @param chatMessage 用户
     * @return
     */
    String checkUser(ChatMessage chatMessage);

    /**
     * 校验用户使用次数
     *
     * @param chatMessage 用户
     * @return
     */
    Boolean checkUserUsage(ChatMessage chatMessage);

    /**
     * 增加用户
     *
     * @param user 用户信息
     */
    void addUser(User user);

    /**
     * 修改用户
     *
     * @param user 用户信息
     */
    void updateUser(User user);

    /**
     * 导出所有用户
     *
     * @param response 响应流
     */
    void exportJob(HttpServletResponse response) throws IOException;


    /**
     * 用户数据excel导入
     * @param file
     */
    void importUser(MultipartFile file) throws IOException;
}
