package com.wrp.chatgpt.modules.user.entity;

import com.alibaba.excel.annotation.ExcelProperty;

/**
 * @author wrp
 */
public class User {

    /**
     * 用户名称
     */
    @ExcelProperty(value = "用户名称")
    private String userName;

    /**
     * 用户剩余使用次数
     */
    @ExcelProperty(value = "用户剩余使用次数")
    private Integer remainingUsage;

    /**
     * 密码
     *
     */
    @ExcelProperty(value = "密码")
    private String password;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getRemainingUsage() {
        return remainingUsage;
    }

    public void setRemainingUsage(Integer remainingUsage) {
        this.remainingUsage = remainingUsage;
    }





}
