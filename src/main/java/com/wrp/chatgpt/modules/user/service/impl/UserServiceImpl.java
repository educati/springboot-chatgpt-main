package com.wrp.chatgpt.modules.user.service.impl;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.EasyExcelFactory;
import com.wrp.chatgpt.modules.user.listener.ImportUserListener;
import com.wrp.chatgpt.modules.gpt.entity.ChatMessage;
import com.wrp.chatgpt.modules.user.entity.User;
import com.wrp.chatgpt.modules.user.service.UserService;
import com.wrp.chatgpt.util.EasyExcelUtil;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 用户业务逻辑
 *
 * @author wrp
 */
@Service
public class UserServiceImpl implements UserService {

    private static HashMap<String, User> map = new HashMap<>();


    /**
     * 每次启动都定义好管理员
     */
    static {
        User admin = new User();
        admin.setUserName("admin");
        admin.setRemainingUsage(1000);
        admin.setPassword("123456");
        map.put("admin", admin);
    }

    /**
     * 判断用户是否具有使用行为
     *
     * @param chatMessage 用户
     * @return
     */
    @Override
    public String checkUser(ChatMessage chatMessage) {

        if (ObjectUtil.isEmpty(map.get(chatMessage.getSender()))) {
            return "用户不存在，请联系管理员";
        }

        User user = map.get(chatMessage.getSender());

        if (user.getRemainingUsage() <= 0) {
            return "用户使用次数不足，请联系管理员";
        }

        if (!chatMessage.getPassword().equals(user.getPassword())) {
            return "用户密码不对，请重新登录";
        }

        return "欢迎尊敬的保时捷车主使用chatGpt,祝你生活愉快";
    }


    /**
     * 校验用户使用次数
     *
     * @param chatMessage 用户
     * @return
     */
    @Override
    public synchronized Boolean checkUserUsage(ChatMessage chatMessage) {

        User user = map.get(chatMessage.getSender());

        if (BeanUtil.isEmpty(user)) {
            return false;
        }

        Integer remainingUsage = user.getRemainingUsage() - 1;

        user.setRemainingUsage(remainingUsage);

        map.remove(chatMessage.getSender());

        map.put(chatMessage.getSender(), user);

        if (remainingUsage < 0) {
            return false;
        }

        return true;
    }


    /**
     * 增加用户
     *
     * @param user 用户信息
     */
    @Override
    public void addUser(User user) {
        if (StrUtil.isBlank(user.getPassword()) && StrUtil.isBlank(user.getUserName())) {
            throw new RuntimeException("请填写用户名和密码");
        }

        //校验用户名
        if (BeanUtil.isNotEmpty(map.get(user.getUserName()))) {
            throw new RuntimeException("用户名已经存在");
        }

        //放到缓存
        map.put(user.getUserName(), user);

        //持久化到excel

    }


    /**
     * 修改用户
     *
     * @param user 用户信息
     */
    @Override
    public void updateUser(User user) {
        //校验用户名
        if (BeanUtil.isEmpty(map.get(user.getUserName()))) {
            throw new RuntimeException("用户名不存在");
        }

        map.remove(user.getUserName());

        //放到缓存
        map.put(user.getUserName(), user);
    }


    /**
     * 导出所有用户
     *
     * @param response
     */
    @Override
    public void exportJob(HttpServletResponse response) throws IOException {
        List<User> userList = map.values().stream().collect(Collectors.toList());
        OutputStream outputStream = EasyExcelUtil.getServletOutputStream(response, "用户数据");
        EasyExcelFactory.write(outputStream).head(User.class).sheet("用户数据").doWrite(userList);
    }



    /**
     * 用户数据excel导入
     * @param file
     */
    @Override
    public void importUser(MultipartFile file) throws IOException {

        //缓存名称key
        String cacheKey = String.valueOf(System.currentTimeMillis());
        try {

            //1.获取输入流
            InputStream in = file.getInputStream();

            //2.解析EXCEL数据
            EasyExcel.read(in, User.class, new ImportUserListener(cacheKey)).sheet().headRowNumber(1).autoTrim(true).doRead();

            //3.获取到EXCEL数据
            List<User> userList =  ImportUserListener.cache.get(cacheKey);
            map.clear();

            Map<String, User> specialMap = userList.stream().collect(Collectors.toMap(User::getUserName, vo -> vo, (v1, v2) -> v1));
            map.putAll(specialMap);


            //4.获取当前缓存的用户数据进行对比
//            List<User> users = map.values().stream().collect(Collectors.toList());

            //根据用户名称取从excel里面获取到的数据和当前缓存数据的并集



        }catch (Exception e){

        }finally {
            //最后一定要移除，防止oom
            ImportUserListener.cache.remove(cacheKey);

        }






    }


}
