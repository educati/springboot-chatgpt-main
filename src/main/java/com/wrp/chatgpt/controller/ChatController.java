package com.wrp.chatgpt.controller;
import com.wrp.chatgpt.modules.gpt.service.ChatGptService;
import com.wrp.chatgpt.modules.user.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import com.alibaba.fastjson.JSON;
import com.wrp.chatgpt.modules.gpt.entity.ChatMessage;

@Controller
public class ChatController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ChatController.class);

    @Autowired
    private ChatGptService chatGptService;
    
    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @Autowired
    private UserService userService;

    @MessageMapping("/chat.sendMessage")
    public void sendMessage(@Payload ChatMessage chatMessage) {
        try {
        	LOGGER.info("sendMessage:{}",JSON.toJSONString(chatMessage));
            Boolean  checkUserUsage =  userService.checkUserUsage(chatMessage);
            if(checkUserUsage) {
                messagingTemplate.convertAndSend("/topic/public/"+chatMessage.getRandom(), new ChatMessage(chatGptService.message(chatMessage.getContent())));
            }else {
                messagingTemplate.convertAndSend("/topic/public/"+chatMessage.getRandom(), new ChatMessage("使用次数不足，请联系管理员充值"));
            }
        } catch (Exception e) {
            LOGGER.info("报错：：{}",e.getMessage()+e.getCause());
        }
    }

    @MessageMapping("/chat.addUser")
    public void addUser(@Payload ChatMessage chatMessage, SimpMessageHeaderAccessor headerAccessor) {

        LOGGER.info("User added in Chat:{}" , chatMessage.getSender());
        try {
            headerAccessor.getSessionAttributes().put("username", chatMessage.getSender());
            messagingTemplate.convertAndSend("/topic/public/"+chatMessage.getRandom(),new ChatMessage(userService.checkUser(chatMessage)));
            System.out.println("addUser:"+JSON.toJSONString(chatMessage));
        } catch (Exception e) {
            LOGGER.info("报错：：{}",e.getMessage()+e.getCause());
        }
    }

}
