package com.wrp.chatgpt.controller;


import com.wrp.chatgpt.config.HashedWheelTimerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * 测试控制层
 * @author wrp
 */
@RestController
public class TestController {

    private Logger logger = LoggerFactory.getLogger(TestController.class);

    @Autowired
    private HashedWheelTimerConfig hashedWheelTimerConfig;


//    @Scheduled(fixedRate = 10000)
//    public void outputLogger() {
//        logger.info("测试日志输出" +"DDDDDDDD");
//        //向时间轮中添加一个3秒的延时任务
//        hashedWheelTimerConfig.hashedWheelTimer().newTimeout(task -> {
//            //注意这里使用异步任务线程池或者开启线程进行订单取消任务的处理
//            cancelOrder(Thread.currentThread().getName());
//        }, 3, TimeUnit.SECONDS);
//    }
//
//
//
//    @Async
//    public void cancelOrder(String name){
//        logger.info(name);
//        logger.info("1111111111111111111111111111111111");
//    }


}
