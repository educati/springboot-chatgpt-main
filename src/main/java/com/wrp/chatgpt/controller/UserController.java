package com.wrp.chatgpt.controller;

import com.wrp.chatgpt.modules.user.entity.User;
import com.wrp.chatgpt.modules.user.service.UserService;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * 用户管理
 * @author wrp
 */
@RestController
@RequestMapping("/api/user")
@Api(value = "用户管理", tags = "用户管理")
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private  UserService userService;



    @PostMapping("/addUser")
    @ApiOperation(value = "新增用户")
    public void addUser(@RequestBody User user) {
        userService.addUser(user);
    }


    @PostMapping("/updateUser")
    @ApiOperation(value = "更新用户")
    public void updateUser(@RequestBody User user) {
        userService.updateUser(user);
    }

    @GetMapping("/export")
    @ApiOperation(value = "导出所有用户")
    public void exportJob(HttpServletResponse response) throws IOException {
        userService.exportJob(response);
    }


    /**
     * 用户数据excel导入
     * @param file
     * @return
     */
    @ApiOperation(value = "用户数据excel导入", notes = "用户数据excel导入", httpMethod = "POST")
    @PostMapping(value = "/importUser")
    public void importUser(@ApiParam(value = "Excel导入数据", required = true) @RequestParam("file") MultipartFile file) throws IOException {
        userService.importUser(file);
    }
}
